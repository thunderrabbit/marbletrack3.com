+++
title = "el Lifty Lever"
type = "parts"
tags = [ "ell", "lever" ]
image = "https://b.robnugen.com/art/marble_track_3/track/parts/2019/2019_nov_19_top_view_el_Lifty_Lever.jpg"
thumbnail = "https://b.robnugen.com/art/marble_track_3/track/parts/2019/thumbs/2019_nov_19_top_view_el_Lifty_Lever.jpg"
description = "el Lifty Lever"
workers = [
    "mr_mcglue",
    "g_choppy",
	"mr_greene"
]
aliases = [
    "/p/eLL"
]
date = 2019-11-19T17:04:27+09:00
+++


History:

* 2019 Nov 19: [Mr Greene](/workers/mr_greene/) started carrying it onto the set.  Jimmy helped name the piece.  I had tentatively called it Large Marble Liftee.
