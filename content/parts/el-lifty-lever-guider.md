+++
title = "el Lifty Lever Guider"
type = "parts"
tags = [ "horizontal", "bamboo", "guideway", "2020" ]
image = "https://b.robnugen.com/art/marble_track_3/track/parts/2019/2020_may_19_reversible_guy_installing_el_lifty_lever_guider.jpg"
thumbnail = "https://b.robnugen.com/art/marble_track_3/track/parts/2019/thumbs/2020_may_19_reversible_guy_installing_el_lifty_lever_guider.jpg"
description = "el Lifty Lever Guider"
workers = [
    "backpack_jack",
    "reversible",
    "g_choppy"
]
aliases = [
    "/p/ellg"
]
date = 2020-05-19T14:36:13+09:00
+++

This is a composite piece made out of a rectangular bamboo skewer.

History:

* 2020 May 19: [Reversible Guy](/workers/reversible/) installing [el Lifty Lever Guider](/parts/el-lifty-lever-guider/)

[![2020 may 19 reversible guy installing el lifty lever guider](//b.robnugen.com/art/marble_track_3/track/parts/2019/thumbs/2020_may_19_reversible_guy_installing_el_lifty_lever_guider.jpg)](//b.robnugen.com/art/marble_track_3/track/parts/2019/2020_may_19_reversible_guy_installing_el_lifty_lever_guider.jpg)

* previously: [G Choppy](/workers/g_choppy/) finally cut the skewer

* previously: [G Choppy](/workers/g_choppy/) floated around the track trying to cut the skewer

* previously: [Backpack Jack](/workers/backpack_jack/) brought a skewer onto the stage
