+++
title = "Lower Zig Zag 1 ban"
type = "parts"
tags = [ "lzz1b", "lzz" ]
image = "https://b.robnugen.com/art/marble_track_3/construction/2018/2018_nov_08_lower_zig_zag_ready_to_test.jpg"
thumbnail = "https://b.robnugen.com/art/marble_track_3/construction/2018/thumbs/2018_nov_08_lower_zig_zag_ready_to_test.jpg"
description = "Lowest track of the Lower Zig Zag"
workers = [
  "mr_mcglue",
  "dr_sugar",
  "mr_greene",
  "autosticks"
]
aliases = [
    "/p/lzz1b"
]
date = 2019-05-05T21:48:13+09:00
+++


History:

* 2019 May 05: recently named lowest track of the [Lower Zig Zag](/parts/lower_zig_zag/)
